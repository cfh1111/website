---
title: 团队介绍
keywords: soul-team
description: 团队介绍
---

### 团队成员（排名不分先后）

 |名字                      | github  |  角色   | 所在公司  |
 |:------------------------ |:----- |:-------: |:-------:|
 |肖宇                |yu199195 |  VP   | 京东     |
 |张永伦                |tuohai666 |  committer   | 京东     |
 |陈斌                |prFor |  committer   | 某创业公司     |
 |蒋晓峰                |SteNicholas |  committer   | 阿里云     |
 |黄晓峰                |huangxfchn |  committer   | shein     | 
 |梁自强                |241600489 |  committer   | 滴滴     | 
 |汤昱东              |tydhot      |  committer   | [perfma](https://perfma.com/)       |
 |邓力铭                |dengliming |  committer   | 某创业公司     | 
 |张磊                |SaberSola |  committer   | 哈罗     | 