---
title: Team Introduction
keywords: soul-team
description: Team Introduction
---

### Team Member（In no particular order）

 |Name                      | Github  |  Role   | Company  |
 |:------------------------ |:----- |:-------: |:-------:|
 |Xiao Yu                |yu199195 |  VP   | [jd.com](https://jd.com)     |
 |Zhang Yonglun                |tuohai666 |  committer   | [jd.com](https://jd.com)     |
 |Chen Bing                |prFor |  committer   | a start-up company     |
 |Jiang Xiaofeng                |SteNicholas |  committer   | [aliyun](https://www.aliyun.com)   |
 |Huang Xiaofeng                |huangxfchn |  committer   | [shein](https://www.shein.com.hk)     | 
 |Liang Ziqiang                |241600489 |  committer   | [didi](https://www.didiglobal.com/)     | 
 |Tang Yudong                  |tydhot      |  committer   | [perfma](https://perfma.com/)       |
 |Deng Liming                  |dengliming |  committer   | a start-up company     | 
 |Zhang lei                |SaberSola |  committer   | [helloglobal](https://www.helloglobal.com/)     | 